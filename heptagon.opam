opam-version: "2.0"
name: "heptagon"
version: "1.05.00"
synopsis: "Compiler for the Heptagon/BZR synchronous programming language"
description: "Heptagon/BZR is a synchronous dataflow language whose syntax and
semantics is inspired from Lustre, with a syntax allowing the
expression of control structures (e.g., switch or mode automata).
Heptagon/BZR is a research compiler, whose aim is to facilitate
experimentation. The current version of the compiler includes the
following features:
- Inclusion of discrete controller synthesis within the compilation
- Expression and compilation of array values with modular memory optimization
See https://gitlab.inria.fr/synchrone/heptagon for further informations."
maintainer: "Gwenaël Delaval <gwenael.delaval@inria.fr>"
authors: "Gwenaël Delaval <gwenael.delaval@inria.fr>"
homepage: "https://gitlab.inria.fr/synchrone/heptagon"
dev-repo: "git+https://gitlab.inria.fr/synchrone/heptagon.git"
bug-reports: "https://gitlab.inria.fr/synchrone/heptagon/-/issues"
license: [" GPL-3.0-or-later"]
build: [
   ["./configure" "--prefix" prefix]
   [make]
]
depends: [
  "ocaml"      {build & >= "4.03.0" }
  "ocamlfind"  {build}
  "menhir"     {build & >= "20141215"}
  "ocamlgraph" {build}
  "camlp4"     {build}
  "ocamlbuild" {build}
]
depopts: [
  "lablgtk"
  "reatk"
]
conflicts: [
  "reatk" { <= "0.14" }
]

